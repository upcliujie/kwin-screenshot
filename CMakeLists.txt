cmake_minimum_required(VERSION 3.15)

set(CMAKE_CXX_STANDARD 20)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

project(kwin-screenshot)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets DBus Concurrent REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets DBus Concurrent REQUIRED)

# Add the executable
add_executable(kwin-screenshot main.cpp widget.cpp)

# Link the Qt5::Widgets target to the executable
target_link_libraries(kwin-screenshot PRIVATE Qt${QT_VERSION_MAJOR}::Widgets Qt${QT_VERSION_MAJOR}::DBus Qt${QT_VERSION_MAJOR}::Concurrent)